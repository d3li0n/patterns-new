<?php


abstract class Builder
{
    // Шаблонный метод
    final public function build()
    {
        $this->test();
        $this->lint();
        $this->assemble();
        $this->deploy();
    }

    abstract public function test();
    abstract public function lint();
    abstract public function assemble();
    abstract public function deploy();
}

class RubyApp extends Builder
{
    public function test()
    {
        // TODO: Implement test() method.
    }

    public function lint()
    {
        // TODO: Implement lint() method.
    }

    public function assemble()
    {
        // TODO: Implement assemble() method.
    }

    public function deploy()
    {
        // TODO: Implement deploy() method.
    }
}

class PhpApp extends Builder
{
    public function test()
    {
        // TODO: Implement test() method.
    }

    public function lint()
    {
        // TODO: Implement lint() method.
    }

    public function assemble()
    {
        // TODO: Implement assemble() method.
    }

    public function deploy()
    {
        // TODO: Implement deploy() method.
    }
}


$ruby = new RubyApp();
$php = new PhpApp();

$php->build();
$ruby->build();