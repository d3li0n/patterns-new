<?php

interface Observer
{
   public function onJobPosted(JobPost $job);
}

class JobPost
{
    protected $title;

    public function __construct(string $title)
    {
        $this->title = $title;
    }

    public function getTitle()
    {
        return $this->title;
    }
}

class JobSeeker implements Observer
{
    protected $name;

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    public function onJobPosted(JobPost $job)
    {
        echo 'Привет ' . $this->name . '! Появилась новая работа: '. $job->getTitle();
    }
}

class Recruiter
{
    public $seekers = [];

    public function addSeeker(Observer $seeker)
    {
        $this->seekers[] = $seeker;
    }

    public function addVacancy(string $job)
    {
        /**
         * @var Observer $seeker
         */
        foreach ($this->seekers as $seeker) {
            $seeker->onJobPosted(new JobPost($job));
        }
    }
}

$recruiter = new Recruiter();

$recruiter->addSeeker(new JobSeeker('Vasya'));
$recruiter->addSeeker(new JobSeeker('Pupkin'));

$recruiter->addVacancy('Tester');
