<?php

class EditorMemento
{
    protected $content;

    public function __construct(string $content)
    {
        $this->content = $content;
    }

    public function getContent()
    {
        return $this->content;
    }
}

class Editor
{
    private $text;

    public function addText(string $text)
    {
        $this->text .= $text;
    }

    public function save()
    {
        return new EditorMemento($this->text);
    }

    public function restore(EditorMemento $content)
    {
        return $content->getContent();
    }

    public function show()
    {
        return $this->text;
    }
}

$photoshop = new Editor();

$photoshop->addText('Test');
echo $photoshop->show() . PHP_EOL; //Test

$test = $photoshop->save();

$photoshop->addText('Test23');
echo $photoshop->show() . PHP_EOL; //TestTest23

echo $photoshop->restore($test) . PHP_EOL; //Test