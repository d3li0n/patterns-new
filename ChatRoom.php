<?php

interface ChatRoomMediator
{
    public function showMessage(User $user, string $message);
}

// Посредник
class Telegram implements ChatRoomMediator
{
    public function showMessage(User $user, string $message)
    {
        $time = date('M d, y H:i');
        $sender = $user->getName();

        echo $time . '[' . $sender . ']:' . $message;
    }
}

class User
{
    private $name;
    private $messenger;

    public function __construct(string $name, Telegram $messenger)
    {
        $this->name = $name;
        $this->messenger = $messenger;
    }

    public function sendMessage(string $message): string
    {
        return $message;
    }

    public function getName(): string
    {
        return $this->name;
    }
}

$messenger = new Telegram();

$vasya = new User('Vasya', $messenger);
$anton = new User('Anton', $messenger);

$message = 'Privet';

$vasya->sendMessage($message);
$messenger->showMessage($vasya, $message);
