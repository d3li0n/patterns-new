<?php

interface SortStrategy
{
    public function sort(array $dataset): array;
}

class BubbleSortStrategy implements SortStrategy
{
    public function sort(array $dataset): array
    {
        echo "Сортировка пузырьком";

        // Сортировка
        return $dataset;
    }
}

class QuickSortStrategy implements SortStrategy
{
    public function sort(array $dataset): array
    {
        echo "Быстрая сортировка";

        // Сортировка
        return $dataset;
    }
}

class Sorter
{
    private $strategy;

    public function __construct(SortStrategy $strategy)
    {
        $this->strategy = $strategy;
    }

    public function sort(array $dataset)
    {
        $this->strategy->sort($dataset);
    }
}

$quick = new QuickSortStrategy();
$bulb = new BubbleSortStrategy();

$sorter = new Sorter($quick);
$sorter->sort([1,3,6,3,4,3,23,2,32]);

$sorterTwo = new Sorter($bulb);
$sorterTwo->sort([3,52,16,2135,123512,2561231,2]);