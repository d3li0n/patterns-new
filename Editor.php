<?php

interface WritingState
{
    public function write(string $words);
}

class UpperCase implements WritingState
{
    public function write(string $words)
    {
        echo strtoupper($words);
    }
}

class LowerCase implements WritingState
{
    public function write(string $words)
    {
        echo strtolower($words);
    }
}

class Original implements WritingState
{
    public function write(string $words)
    {
        echo $words;
    }
}

class Editor
{
    private $state;

    public function __construct(WritingState $state)
    {
        $this->state = $state;
    }

    public function show(string $text): string
    {
        return $this->state->write($text);
    }

    public function setState(WritingState $state): void
    {
        $this->state = $state;
    }
}


$editor = new Editor(new Original());

$editor->show('Test'); //Test

$editor->setState(new UpperCase());
$editor->show('Test'); //TEST