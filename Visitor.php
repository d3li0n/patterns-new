<?php

//Тот, кого посещают
interface Animal
{
    public function accept(AnimalOperation $operation);
}

// Посетитель
interface AnimalOperation
{
    public function visitMonkey(Monkey $monkey);
    public function visitLion(Lion $lion);
}

class Monkey implements Animal
{
    public function shout()
    {
        echo 'У-у-а-а!';
    }

    public function accept(AnimalOperation $operation)
    {
        $operation->visitMonkey($this);
    }
}

class Lion implements Animal
{
    public function roar()
    {
        echo 'рррр!';
    }

    public function accept(AnimalOperation $operation)
    {
        $operation->visitLion($this);
    }
}

class Reaction implements AnimalOperation
{
    public function visitLion(Lion $lion)
    {
        $lion->roar();
    }

    public function visitMonkey(Monkey $monkey)
    {
        $monkey->shout();
    }
}


$lion = new Lion();
$monkey = new Monkey();
$reaction = new Reaction();

$monkey->accept($reaction);
$lion->accept($reaction);