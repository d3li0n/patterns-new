<?php

class Bulb
{
    public function turnOn()
    {
        echo "Лампочка загорелась";
    }

    public function turnOff()
    {
        echo "Темнота!";
    }
}

interface Command
{
    public function execute();
    public function undo();
    public function redo();
}

class SwitchOn implements Command
{
    private $bulb;

    public function __construct(Bulb $bulb)
    {
        $this->bulb = $bulb;
    }

    public function execute()
    {
        $this->bulb->turnOn();
    }

    public function redo()
    {
        $this->execute();
    }

    public function undo()
    {
        $this->bulb->turnOff();
    }
}

class SwitchOff implements Command
{
    private $bulb;

    public function __construct(Bulb $bulb)
    {
        $this->bulb = $bulb;
    }

    public function execute()
    {
        $this->bulb->turnOff();
    }

    public function redo()
    {
        $this->execute();
    }

    public function undo()
    {
        $this->bulb->turnOn();
    }
}

class Human
{
    public function submit(Command $command)
    {
        $command->execute();
    }
}

$bulb = new Bulb();

// Electricity
$commandOn = new SwitchOn($bulb);
$commandOff = new SwitchOff($bulb);

$commandOn->redo();
$commandOff->redo();

// Human does staff
$human = new Human();

$human->submit($commandOn);
$human->submit($commandOff);